#!/usr/bin/env python

from datetime import datetime
from sys import stdin, stderr, stdout

def read_input(file, today):


	for line in file:
		try:
			inp = line[:-1].split('\t')

			if inp[0] == 'old':
				cookie, segsWithDate = inp[1], inp[2].split(' ')

				for segs in segsWithDate:
					todayDate = datetime.strptime(today, "%Y%m%d").date()
					segDate = datetime.strptime(segs[:segs.find(':')], "%Y%m%d").date()

					if (todayDate - segDate).days > 30:
						yield '%s\t%s\t%s\n' % (cookie, 'rem', segs[segs.find(':')+1:])

					else:
						yield '%s\t%s\t%s\n' % (cookie, 'new', segs)

			elif inp[0] == 'new':
				cookie, segs = inp[1], inp[2]
				yield '%s\t%s\t%s\n' % (cookie, 'new', ':'.join((today, segs)))

		except (ValueError, AttributeError):
			stderr.write('ValueError\n')
			yield False


try:
    today = argv[1]
except:
    today = '20160501'

any(stdout.write(i) for i in read_input(stdin, today) if i)
