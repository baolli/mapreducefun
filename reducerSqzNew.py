#!/usr/bin/env python

from sys import stdin, stdout, stderr

def extract(file):

    currentCookie, out, segsNew, segsRem, segsFromDate, oldDate, outTrue, segsDate, segsNewTrue = '', '', set([]), set([]), set([]), '', '', '', ''

    for line in file:
        try:
            cookie, ttype, data = line[:-1].split('\t')

            if cookie != currentCookie:

                segsDate =  '/'.join(sorted(segsFromDate))
                if segsDate:
                    out = ':'.join((oldDate, segsDate))  
                if outTrue:              
                    outTrue = ' '.join((out, outTrue))

                if outTrue:
                    yield '%s\t%s\t%s\n' % ('new', currentCookie, ''.join(outTrue)[:-3])
                if (segsRem - segsNew):
                    yield '%s\t%s\t%s\n' % ('rem', currentCookie, '/'.join(sorted(segsRem - segsNew)))

                currentCookie, segsNew, segsRem, outTrue, segsNewTrue, segsDate, segsFromDate, oldDate, out = cookie, set([]), set([]), '', set([]), '', '', '', ''

            if ttype == 'rem':
                segsRem = segsRem | set(data.split('/'))

            if ttype == 'new':
                currentDate = data.split(':')[0]
                segsNewTrue = set(data.split(':')[-1].split('/'))
                segsNew = segsNew | segsNewTrue

                if currentDate != oldDate:
                    segsDate =  '/'.join(sorted(segsFromDate))
                    out = ':'.join((oldDate, segsDate))

                    if out:
                        outTrue = ' '.join((out, outTrue))

                    oldDate, segsDate, segsFromDate, out = currentDate, '', set([]), ''

                segsFromDate = segsFromDate | segsNewTrue
                oldDate = currentDate

        except:
            stderr.write("error\n")
            continue

any(stdout.write(i) for i in extract(stdin) if i)